# Panduan Instalasi SISURAT Fakultas Farmasi UI

Anda dapat membaca panduan instalasi proyek ini pada [link ini](https://docs.google.com/document/d/1hRt5hu7DqCakOHsS2xxXKfn_Q6sLQ8wk/edit?usp=sharing&ouid=100041255579456996755&rtpof=true&sd=true).

* * *

Kelompok Propensi C03 - SISURAT  
Klien: Fakultas Farmasi Universitas Indonesia  
Dosen:Dr. Panca Oktavia Hadi Putra B.Sc., M.Bus.  
Asisten dosen: Karin Patricia  

Anggota:

- Najla Laila Muharram - Project Manager - 1906399612
- Cut Zahra Nabila - Lead Analyst - 1906399934
- Ahmad Harori Zaki Ichsan - Lead Programmer - 1906353965
- Reno Fathoni - Scrum Master - 1906399461
- Fareeha Nisa Zayda Azeeza - Lead Designer - 1906399644