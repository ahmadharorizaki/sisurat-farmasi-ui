$(document).ready(function () {
    const typePlaceholder = $(".type-placeholder")

    typePlaceholder.each(function () {
        const label = $(this).parent().prev().find("label");
        const input = $(this).parent().prev().find("input");
        switch (this.value) {
            case "list":
                label.text("Nama-Nama Kolom (dipisah dengan koma)")
                input.attr("placeholder", "Nama Bahan,Jenis Bahan,Berat (gram)")
                break
            case "date":
                label.text("Nama Isian")
                input.attr("placeholder", "Tanggal Mulai")
                break
            case "month":
                label.text("Nama Isian")
                input.attr("placeholder", "Bulan Mulai")
                break
            case "text":
                label.text("Nama Isian")
                input.attr("placeholder", "No Hp")
                break
        }
    })

    typePlaceholder.change(function () {
        const label = $(this).parent().prev().find("label");
        const input = $(this).parent().prev().find("input");
        switch (this.value) {
            case "list":
                label.text("Nama-Nama Kolom (dipisah dengan koma)")
                input.attr("placeholder", "Nama Bahan,Jenis Bahan,Berat (gram)")
                break
            case "date":
                label.text("Nama Isian")
                input.attr("placeholder", "Tanggal Mulai")
                break
            case "month":
                label.text("Nama Isian")
                input.attr("placeholder", "Bulan Mulai")
                break
            case "text":
                label.text("Nama Isian")
                input.attr("placeholder", "No Hp")
                break
        }
    })
})


