document.getElementById('okeSearch').onclick = function() {
    const search = document.getElementById('name').value;
    const base_url = "https://sisurat-farmasi-ui.herokuapp.com";
    $.ajax({
        type: "POST",
        contentType : 'application/json; charset=utf-8',
        dataType : 'json',
        url: base_url + "/permintaanSurat/?search=" + search,
        asynch: true,
        success : function (data){
            var str = ""
            $('table tbody').html('')
            $('table tbody').html('')
            $('#none').html('')
            if (data.result.length == 0){
                none = '<br>'+ '<br>' + '<h5 style="text-align:center; margin-left:50px"> Tidak terdapat permintaan surat dengan kata kunci tersebut.</h5>';
                $('#none').append(none);
            } else {
                for (var i = 0; i < data.result.length; i++) {
                    var number = i+1;
                    str = '<tr>' +  '<td>' + number + '</td>' +
                        '<td>' + data.result[i].jenisSurat + '</td>' +
                        '<td>' + data.result[i].nama + '</td>' +
                        '<td>' + data.result[i].waktuDiajukan + '</td>' +
                        '<td>' + data.result[i].bahasaSurat + '</td>' +
                        '<td>' + data.result[i].bentukSurat + '</td>' +
                        '<td>' + data.result[i].status + '</td>' +
                        '<td>' +
                        '<div className="align-items-center">' +
                        '<a href="/detailSurat/' + data.result[i].idPermintaanSurat + '" class="btn btn-primary">Lihat Detail</a>' +
                        '</div>' +
                        '</td>' +
                        '</tr>';
                    $('table tbody').append(str);
                }
            }
        }
    });

}