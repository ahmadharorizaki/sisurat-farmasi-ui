package ui.farmasi.sisurat;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication
public class SisuratApplication {

	public static void main(String[] args) {
		SpringApplication.run(SisuratApplication.class, args);
	}

}
