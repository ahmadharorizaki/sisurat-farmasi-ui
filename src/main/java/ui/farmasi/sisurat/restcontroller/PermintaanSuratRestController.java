package ui.farmasi.sisurat.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ui.farmasi.sisurat.form.Data;
import ui.farmasi.sisurat.model.*;
import ui.farmasi.sisurat.rest.BaseResponse;
import ui.farmasi.sisurat.rest.PermintaanSuratDTO;
import ui.farmasi.sisurat.service.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
public class PermintaanSuratRestController {
    @Qualifier("permintaanSuratServiceImpl")
    @Autowired
    private PermintaanSuratService permintaanSuratService;

    @Qualifier("mahasiswaServiceImpl")
    @Autowired
    private MahasiswaService mahasiswaService;

    @Qualifier("eksekutifServiceImpl")
    @Autowired
    private EksekutifService eksekutifService;

    @Qualifier("userServiceImpl")
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/permintaanSurat")
    public BaseResponse<List<PermintaanSuratDTO>> listRequestSurat(
           HttpServletRequest request, @RequestBody(required = false) Data data
    ){
        BaseResponse<List<PermintaanSuratDTO>> response = new BaseResponse<>();
        Byte status;
        if (data.Id != 0){
            Integer temp = data.Id - 1;
            status = temp.byteValue();
        } else {
            status = 5;
        }

        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());
        RoleModel role = user.getRole();
        List<PermintaanSuratModel> listRequest = new ArrayList<>();
        if (role.getNamaRole().equals("ADMIN")) {
            if (status != 5){
                for (PermintaanSuratModel surat: permintaanSuratService.getListPermintaanByProdi(user.getProdi())
                ) {
                    if(surat.getStatus() == status){
                        listRequest.add(surat);
                    }
                }
            } else {
                listRequest = permintaanSuratService.getListPermintaanByProdi(user.getProdi());
            }
        }

        if (role.getNamaRole().equals("EKSEKUTIF")){
            EksekutifModel eksekutif = eksekutifService.getEksekutifById(user.getId());
            if (status != 5){
                for (PermintaanSuratModel surat: eksekutif.getListPermintaanSurat()
                ) {
                    if(surat.getStatus() == status){
                        listRequest.add(surat);
                    }
                }
            } else {
                listRequest = eksekutif.getListPermintaanSurat();
            }

        }
        Collections.reverse(listRequest);
        List<PermintaanSuratDTO> listRequestDTO = new ArrayList<>();
        for (PermintaanSuratModel surat: listRequest
             ) {
            listRequestDTO.add(PermintaanSuratDTO.convertToDTO(surat));
        }
        response.setStatus(200);
        response.setMessage("success");
        response.setResult(listRequestDTO);
        return response;
    }

    @RequestMapping(value = "/permintaanSurat",params = "search")
    public BaseResponse<List<PermintaanSuratDTO>> listRequestSuratSearch(
            HttpServletRequest request, @RequestParam("search") String search
    ){
        BaseResponse<List<PermintaanSuratDTO>> response = new BaseResponse<>();
        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());
        RoleModel role = user.getRole();
        List<PermintaanSuratModel> listRequest = new ArrayList<>();

        if (role.getNamaRole().equals("ADMIN")) {
            List <MahasiswaModel> listMahasiswa = mahasiswaService.getMahasiswaByNama(search);
            for (MahasiswaModel mahasiswa: listMahasiswa
                 ) {
                if (mahasiswa.getProdi().equals(user.getProdi())){
                    for (PermintaanSuratModel surat : permintaanSuratService.getListPermintaanSuratMahasiswa(mahasiswa)
                         ) {
                        listRequest.add(surat);
                    }
                }

            }

        }

        if (role.getNamaRole().equals("EKSEKUTIF")){
            EksekutifModel eksekutif = eksekutifService.getEksekutifById(user.getId());
            List <MahasiswaModel> listMahasiswa = mahasiswaService.getMahasiswaByNama(search);

                for (PermintaanSuratModel surat : eksekutif.getListPermintaanSurat()
                ) {
                    if (listMahasiswa.contains(surat.getMahasiswa())){
                        listRequest.add(surat);
                    }

                }
        }
        Collections.reverse(listRequest);
        List<PermintaanSuratDTO> listRequestDTO = new ArrayList<>();
        for (PermintaanSuratModel surat: listRequest
        ) {
            listRequestDTO.add(PermintaanSuratDTO.convertToDTO(surat));
        }
        response.setStatus(200);
        response.setMessage("success");
        response.setResult(listRequestDTO);
        return response;

    }
}

