package ui.farmasi.sisurat.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListFieldForm {

    private String name;

    private Integer listId;

    private List<String> values;

    public ListFieldForm(){}

    public ListFieldForm(String name, Integer listId) {
        this.name = name;
        this.listId = listId;
        this.values = new ArrayList<>();
        this.values.add("");
    }

    public ListFieldForm(String name, Integer listId, String value) {
        this.name = name;
        this.listId = listId;
        this.values = csvToList(value);
    }

    private List<String> csvToList(String value) {
        return Arrays.asList(value.split(","));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public Integer getListId() {
        return listId;
    }

    public void setListId(Integer listId) {
        this.listId = listId;
    }
}
