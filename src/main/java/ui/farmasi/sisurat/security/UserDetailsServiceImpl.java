package ui.farmasi.sisurat.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.UserModel;
import ui.farmasi.sisurat.repository.UserDb;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserDb userDb;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel user = userDb.findByUsername(username);

        return new UserPrincipal(user);
    }
}
