package ui.farmasi.sisurat.service;

import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.MahasiswaModel;

import java.util.List;
import java.util.Optional;


public interface MahasiswaService {
   MahasiswaModel getMahasiswabyID (Long id);
   List<MahasiswaModel> getMahasiswaByNama (String nama);
   MahasiswaModel getMahasiswaByNPM (String npm);
   void delete(MahasiswaModel mahasiswa);
}
