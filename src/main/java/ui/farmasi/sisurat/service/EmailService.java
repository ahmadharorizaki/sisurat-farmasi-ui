package ui.farmasi.sisurat.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.context.Context;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import ui.farmasi.sisurat.model.PermintaanSuratModel;
import ui.farmasi.sisurat.model.RoleModel;
import ui.farmasi.sisurat.model.UserModel;

import javax.mail.MessagingException;

@Service
public class EmailService {

    private final TemplateEngine templateEngine;

    private final JavaMailSender javaMailSender;

    public EmailService(TemplateEngine templateEngine, JavaMailSender javaMailSender) {
        this.templateEngine = templateEngine;
        this.javaMailSender = javaMailSender;
    }

    public void sendMail(UserModel user, PermintaanSuratModel surat, RoleModel role) throws MessagingException {
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("surat", surat);
        String process = "";
        String subject = "";
        if (role.getId() == 3 ){
            process = templateEngine.process("email-eksekutif", context);
            subject = "[SURAT] Notifikasi Surat Masuk Butuh Persetujuan";
        } else if (role.getId() == 1 ){
            process = templateEngine.process("email-mahasiswa", context);
            subject = "[SURAT] Notifikasi Perubahan Status Surat";
        } else {
            if (surat.getStatus() == 0){
                subject = "[SURAT] Notifikasi Permintaan Surat Baru Masuk";
                process = templateEngine.process("email-admin", context);
            } else if (surat.getStatus() == 2) {
                subject = "[SURAT] Notifikasi Surat Disetujui Eksekutif";
                process = templateEngine.process("email-admin-eksekutif", context);
            } else {
                subject = "[SURAT] Notifikasi Surat Ditolak Eksekutif";
                process = templateEngine.process("email-admin-eksekutif-ditolak", context);
            }

        }

        javax.mail.internet.MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setSubject(subject);
        helper.setText(process, true);
        helper.setTo(user.getEmailUI());
        if (user.getEmailNonUI() != null) {
            helper.setTo(user.getEmailNonUI());
        }
        javaMailSender.send(mimeMessage);
    }
}