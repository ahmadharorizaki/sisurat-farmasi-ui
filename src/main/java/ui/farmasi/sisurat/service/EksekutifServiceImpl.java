package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.EksekutifModel;
import ui.farmasi.sisurat.repository.EksekutifDb;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class EksekutifServiceImpl implements EksekutifService{
    @Autowired
    private EksekutifDb eksekutifDb;

    @Override
    public List<EksekutifModel> getAllEksekutif(){
        return eksekutifDb.findAll();
    }

    @Override
    public EksekutifModel getEksekutifById(Long id){
        return eksekutifDb.getEksekutifModelById(id);
    }

    @Override
    public void delete(EksekutifModel eksekutif) {
        eksekutifDb.delete(eksekutif);
    }
}
