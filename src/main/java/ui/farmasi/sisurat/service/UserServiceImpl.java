package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.UserModel;
import ui.farmasi.sisurat.repository.UserDb;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService{

    @Autowired
    UserDb userDb;

    @Override
    public UserModel getByUsername(String username) {
        return userDb.findByUsername(username);
    }

    @Override
    public List<UserModel> getAllUser() {
        return userDb.findAll();
    }

    @Override
    public void saveUser(UserModel userModel) {
        userDb.save(userModel);
    }

    //update user
    @Override
    public UserModel updateUser(UserModel user) {
        return userDb.save(user);
    }

    @Override
    public void deleteUser(UserModel user) {
        userDb.delete(user);
        userDb.flush();
    }


}
