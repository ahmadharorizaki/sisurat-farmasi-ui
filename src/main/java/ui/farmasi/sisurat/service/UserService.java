package ui.farmasi.sisurat.service;

import ui.farmasi.sisurat.model.UserModel;

import java.util.List;

public interface UserService {

    UserModel getByUsername(String username);

    List<UserModel> getAllUser();

    void saveUser(UserModel userModel);

    UserModel updateUser(UserModel user);

    void deleteUser(UserModel user);
}
