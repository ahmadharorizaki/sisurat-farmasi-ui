package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.PermintaanSuratModel;
import ui.farmasi.sisurat.model.RiwayatModel;
import ui.farmasi.sisurat.repository.RiwayatDb;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RiwayatServiceImpl implements RiwayatService{
    @Autowired
    RiwayatDb riwayatDb;

    @Override
    public void addRiwayat (RiwayatModel riwayat){
        riwayatDb.save(riwayat);
    }

    @Override
    public RiwayatModel getRiwayatByIdSurat(PermintaanSuratModel permintaanSurat) {
        return riwayatDb.findByPermintaanSurat(permintaanSurat);
    }
}
