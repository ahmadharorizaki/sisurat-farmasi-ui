package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ui.farmasi.sisurat.model.*;
import ui.farmasi.sisurat.repository.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class FileServiceImpl implements FileService{
    @Autowired
    private FileDb fileDb;

    @Autowired
    private MahasiswaDb mahasiswaDb;

    @Autowired
    private PermintaanSuratDb permintaanSuratDb;

    @Override
    public void addFile(FileModel file) {
        fileDb.save(file);
    }

    @Override
    public void saveFile(MultipartFile file, Long idPermintaanSurat) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        PermintaanSuratModel permintaanSurat = permintaanSuratDb.getById(idPermintaanSurat);
        FileModel fileSurat;

        if (permintaanSurat.getFile() == null) {
            fileSurat = new FileModel();

            fileSurat.setNama(fileName);
            fileSurat.setTipe(file.getContentType());
            fileSurat.setFile(file.getBytes());
            fileSurat.setPermintaanSurat(permintaanSurat);
            permintaanSurat.setFile(fileSurat);
        } else {
            fileSurat = fileDb.findFileModelByPermintaanSurat(permintaanSurat);
            fileSurat.setNama(fileName);
            fileSurat.setTipe(file.getContentType());
            fileSurat.setFile(file.getBytes());
        }
        fileDb.save(fileSurat);
        permintaanSuratDb.save(permintaanSurat);
    }

    @Override
    public FileModel getFileByPermintaanSurat(PermintaanSuratModel permintaanSurat) {
        FileModel filePermintaanSurat = fileDb.findFileModelByPermintaanSurat(permintaanSurat);
        return filePermintaanSurat;
    }
}
