package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.AdminModel;
import ui.farmasi.sisurat.repository.AdminDb;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class AdminServiceImpl implements AdminService{
    @Autowired
    AdminDb adminDb;

    @Override
    public List<AdminModel> getAllAdmin(){
        return adminDb.findAll();
    }

    @Override
    public List<AdminModel> getAllAdminByProdi(String prodi){
        return adminDb.findAllByProdi(prodi);
    }

    @Override
    public void delete(AdminModel adminModel) {
        adminDb.delete(adminModel);
        adminDb.flush();
    }

    @Override
    public AdminModel getByUsername(String username) {
        return adminDb.findByUsername(username);
    }
}
