package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.RoleModel;
import ui.farmasi.sisurat.repository.RoleDb;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class RoleServiceImpl implements RoleService{

    @Autowired
    RoleDb roleDb;

    @Override
    public RoleModel getByName(String name) {
        return roleDb.findByNamaRole(name);
    }

    @Override
    public RoleModel getById(Integer id) {
        return roleDb.getById(id);
    }


}
