package ui.farmasi.sisurat.service;
import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import ui.farmasi.sisurat.model.FileModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;

public interface FileService {

    void addFile(FileModel file);
    void saveFile(MultipartFile file, Long idPermintaanSurat) throws IOException;
    FileModel getFileByPermintaanSurat(PermintaanSuratModel permintaanSurat);


}
