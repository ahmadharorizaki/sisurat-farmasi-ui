package ui.farmasi.sisurat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.security.core.Authentication;

import ui.farmasi.sisurat.model.*;
import ui.farmasi.sisurat.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;


@Controller
public class UserController {

    private final WebClient webClient = WebClient.builder().build();

    @Qualifier("roleServiceImpl")
    @Autowired
    private RoleService roleService;

    @Qualifier("userServiceImpl")
    @Autowired
    private UserService userService;

    @Qualifier("permintaanSuratServiceImpl")
    @Autowired
    private PermintaanSuratService permintaanSuratService;

    @Qualifier("mahasiswaServiceImpl")
    @Autowired
    private MahasiswaService mahasiswaService;

    @Qualifier("eksekutifServiceImpl")
    @Autowired
    private EksekutifService eksekutifService;

    @Qualifier("adminServiceImpl")
    @Autowired
    private AdminService adminService;


    @GetMapping(value = "/profile")
    public String editProfile(
            Model model,
            Principal principal,
            Authentication authentication
    ) {
        UserModel user = userService.getByUsername(principal.getName());
        model.addAttribute("user", user);

        return "edit-profile";
    }

    @GetMapping(value = "/profile/edit")
    public String editProfileForm(
            Model model,
            Principal principal
    ) {
        UserModel user = userService.getByUsername(principal.getName());

        model.addAttribute("user", user);

        return "edit-profile-form";
    }


    @PostMapping("/profile/edit")
    public String editProfileSubmitForm(
            @Valid @ModelAttribute("user") UserModel userModel,
            BindingResult result,
            Model model
    ) {

        if (result.hasErrors()) {
            model.addAttribute("user", userModel);
            return "edit-profile-form";
        }
        UserModel user = userService.getByUsername(userModel.getUsername());
        user.setEmailNonUI(userModel.getEmailNonUI());
        userService.updateUser(user);
        model.addAttribute("user", user);
        return "edit-profile";
    }

    @GetMapping("/user")
    public String listUser(
            Model model,
            Principal principal
    ){
        List <UserModel> listUser = userService.getAllUser();
        model.addAttribute("listUser", listUser);
        return "view-list-user";
    }

    @GetMapping("user/{username}")
    public String viewDetailUser(
            @PathVariable String username,
            HttpServletRequest request,
            Model model
    ){
        UserModel userTarget = userService.getByUsername(username);
        model.addAttribute("user", userTarget);
        return "view-detail-user";
    }

    @PostMapping("user/update")
    public String updateUser(
            @ModelAttribute UserModel user,
            Model model
    ){
        UserModel oldUser = userService.getByUsername(user.getUsername());

        if (oldUser.getRole().getNamaRole().equals("MAHASISWA")) {
            MahasiswaModel mahasiswa = mahasiswaService.getMahasiswabyID(oldUser.getId());
            permintaanSuratService.deleteAllByMahasiswa(mahasiswa);
            mahasiswaService.delete(mahasiswa);
        } else if (oldUser.getRole().getNamaRole().equals("EKSEKUTIF")) {
            EksekutifModel eksekutif = eksekutifService.getEksekutifById(oldUser.getId());
            List<PermintaanSuratModel> listSurat = eksekutif.getListPermintaanSurat();
            for (PermintaanSuratModel permintaanSurat : listSurat) {
                permintaanSurat.getListEksekutif().remove(eksekutif);
                permintaanSuratService.updateSurat(permintaanSurat);
            }
            eksekutifService.delete(eksekutif);
        } else {
            AdminModel admin = adminService.getByUsername(oldUser.getUsername());
            adminService.delete(admin);
        }

        Integer role = user.getRole().getId();
        UserModel newUser;
        if (role == 2) {
            newUser = new AdminModel();
            setNewUser(oldUser, newUser, role);
        } else if (role == 1) {
            newUser = new MahasiswaModel();
            setNewUser(oldUser, newUser, role);
        } else {
            newUser = new EksekutifModel();
            setNewUser(oldUser, newUser, role);
        }

        userService.saveUser(newUser);

        model.addAttribute("user", user);
        return "update-user";
    }

    private void setNewUser(UserModel oldUser, UserModel newUser, Integer roleId) {
        newUser.setUsername(oldUser.getUsername());
        newUser.setName(oldUser.getName());
        newUser.setEmailUI(oldUser.getEmailUI());
        newUser.setEmailNonUI(oldUser.getEmailNonUI());
        newUser.setPassword(oldUser.getPassword());
        newUser.setProdi(oldUser.getProdi());
        newUser.setRole(roleService.getById(roleId));
    }
}
