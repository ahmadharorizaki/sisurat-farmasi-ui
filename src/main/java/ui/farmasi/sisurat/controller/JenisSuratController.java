package ui.farmasi.sisurat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ui.farmasi.sisurat.model.FieldAndTypeModel;
import ui.farmasi.sisurat.model.JenisSuratModel;
import ui.farmasi.sisurat.model.UserModel;
import ui.farmasi.sisurat.service.FieldAndTypeService;
import ui.farmasi.sisurat.service.JenisSuratService;
import ui.farmasi.sisurat.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class JenisSuratController {

    static final Map<String, String> daftarTipe = new HashMap<>();

    @Qualifier("jenisSuratServiceImpl")
    @Autowired
    private JenisSuratService jenisSuratService;

    @Qualifier("fieldAndTypeServiceImpl")
    @Autowired
    private FieldAndTypeService fieldAndTypeService;

    @Qualifier("userServiceImpl")
    @Autowired
    private UserService userService;

    public JenisSuratController() {
        daftarTipe.put("Teks", "text");
        daftarTipe.put("Tanggal-Bulan-Tahun","date");
        daftarTipe.put("Bulan-Tahun", "month");
        daftarTipe.put("Tabel", "list");
    }

    @GetMapping("/tambahJenisSurat")
    private String formTambahJenisSurat(
            Model model
    ) {
        JenisSuratModel jenisSurat = new JenisSuratModel();
        jenisSurat.getFieldsAndTypes().add(new FieldAndTypeModel());
        model.addAttribute("jenisSurat", jenisSurat);
        model.addAttribute("daftarTipe", daftarTipe);
        return "form-tambah-jenis-surat";
    }

    @PostMapping(
            value = "/tambahJenisSurat",
            params = {"simpan"}
    )
    private ModelAndView tambahJenisSurat(
            @ModelAttribute JenisSuratModel jenisSurat,
            HttpServletRequest request,
            Model model
    ) {
        for (FieldAndTypeModel fieldAndType : jenisSurat.getFieldsAndTypes()) {
            fieldAndType.setJenisSurat(jenisSurat);
        }
        String username = request.getUserPrincipal().getName();
        UserModel user = userService.getByUsername(username);
        jenisSurat.setProdi(user.getProdi());

        jenisSuratService.saveJenisSurat(jenisSurat);
        model.addAttribute("templateSurat", jenisSurat.getNama());
        return new ModelAndView("redirect:/listtemplate");
    }

    @PostMapping(
            value = "/tambahJenisSurat",
            params = {"tambahField"}
    )
    private String tambahJenisSuratTambahField(
            @ModelAttribute JenisSuratModel jenisSurat,
            Model model
    ) {
        jenisSurat.getFieldsAndTypes().add(new FieldAndTypeModel());
        model.addAttribute("jenisSurat", jenisSurat);
        model.addAttribute("daftarTipe", daftarTipe);
        return "form-tambah-jenis-surat";
    }

    @PostMapping(
            value = "/tambahJenisSurat",
            params = {"hapusField"}
    )
    private String tambahJenisSuratHapusField(
            @ModelAttribute JenisSuratModel jenisSurat,
            @RequestParam("hapusField") Integer fieldIdx,
            Model model
    ) {
        jenisSurat.getFieldsAndTypes().remove(fieldIdx.intValue());
        model.addAttribute("jenisSurat", jenisSurat);
        model.addAttribute("daftarTipe", daftarTipe);
        return "form-tambah-jenis-surat";
    }

    @GetMapping(value = "/updateJenisSurat/{id}")
    public String formUpdateJenisSurat(
            @PathVariable Integer id,
            Model model
    ) {
        JenisSuratModel jenisSurat = jenisSuratService.getSuratById(id);
        model.addAttribute("jenisSurat", jenisSurat);
        model.addAttribute("daftarTipe", daftarTipe);
        return "form-update-jenis-surat";
    }

    @PostMapping(
            value = "/updateJenisSurat",
            params = {"simpan"}
    )
    private ModelAndView updateJenisSurat(
            @ModelAttribute JenisSuratModel jenisSurat,
            Model model
    ) {
        for (FieldAndTypeModel fieldAndType : jenisSurat.getFieldsAndTypes()) {
            fieldAndType.setJenisSurat(jenisSurat);
        }
        jenisSuratService.saveJenisSurat(jenisSurat);
        model.addAttribute("templateSurat", jenisSurat.getNama());
        return new ModelAndView("redirect:/listtemplate");
    }

    @PostMapping(
            value = "/updateJenisSurat",
            params = {"tambahField"}
    )
    private String updateJenisSuratTambahField(
            @ModelAttribute JenisSuratModel jenisSurat,
            Model model
    ) {
        jenisSurat.getFieldsAndTypes().add(new FieldAndTypeModel());
        model.addAttribute("jenisSurat", jenisSurat);
        model.addAttribute("daftarTipe", daftarTipe);
        return "form-update-jenis-surat";
    }

    @PostMapping(
            value = "/updateJenisSurat",
            params = {"hapusField"}
    )
    private String updateJenisSuratHapusField(
            @ModelAttribute JenisSuratModel jenisSurat,
            @RequestParam("hapusField") Integer fieldIdx,
            Model model
    ) {
        FieldAndTypeModel fieldToDelete = jenisSurat.getFieldsAndTypes().get(fieldIdx);
        if (fieldToDelete.getId() != null) {
            fieldAndTypeService.deleteById(fieldToDelete.getId());
        }
        jenisSurat.getFieldsAndTypes().remove(fieldIdx.intValue());
        model.addAttribute("jenisSurat", jenisSurat);
        model.addAttribute("daftarTipe", daftarTipe);
        return "form-update-jenis-surat";
    }

    @GetMapping("/listtemplate")
    public String listTemplate(Model model, HttpServletRequest request){
        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());

        List<JenisSuratModel> listTemplate = jenisSuratService.getListJenisSuratByProdi(user.getProdi());
        model.addAttribute("listTemplate", listTemplate);
        return "view-list-template-surat";
    }

    @GetMapping("/jenisSurat/delete/{id}")
    public ModelAndView deleteJenisSurat(
            @PathVariable Integer id,
            Model model
    ){
        JenisSuratModel jenis = jenisSuratService.getSuratById(id);
        model.addAttribute( "jenis",jenis);

        model.addAttribute( "Id",jenis.getId());
        jenisSuratService.deleteJenisSurat(jenis);
        return new ModelAndView("redirect:/listtemplate");

    }


}
