package ui.farmasi.sisurat.controller;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ui.farmasi.sisurat.form.FieldForm;
import ui.farmasi.sisurat.form.ListFieldForm;
import ui.farmasi.sisurat.form.SuratForm;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ui.farmasi.sisurat.form.ToEksekutif;
import ui.farmasi.sisurat.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import ui.farmasi.sisurat.service.*;
import ui.farmasi.sisurat.setting.Setting;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.io.FilenameUtils;

@Controller
public class PermintaanSuratController {
    @Qualifier("permintaanSuratServiceImpl")
    @Autowired
    private PermintaanSuratService permintaanSuratService;

    @Qualifier("riwayatServiceImpl")
    @Autowired
    private RiwayatService riwayatService;

    @Qualifier("eksekutifServiceImpl")
    @Autowired
    private EksekutifService eksekutifService;

    @Qualifier("adminServiceImpl")
    @Autowired
    private AdminService adminService;

    @Autowired
    private EmailService emailService;

    @Qualifier("mahasiswaServiceImpl")
    @Autowired
    private MahasiswaService mahasiswaService;

    @Qualifier("jenisSuratServiceImpl")
    @Autowired
    private JenisSuratService jenisSuratService;

    @Qualifier("userServiceImpl")
    @Autowired
    private UserService userService;

    @Qualifier("fileServiceImpl")
    @Autowired
    private FileService fileService;
    private FileModel file;

    @GetMapping("/daftarSurat")
    public String listRequestSurat(
            Model model, HttpServletRequest request
    ){
        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());
        RoleModel role = user.getRole();
        List<PermintaanSuratModel> listRequest = new ArrayList<>();

        if (role.getNamaRole().equals("ADMIN")) {
            String prodi = user.getProdi();
            if (prodi.equals("ADMIN FAKULTAS") || prodi.equals("SPECIAL USER")){
                listRequest = permintaanSuratService.getListPermintaanSurat();
            } else {
                listRequest = permintaanSuratService.getListPermintaanByProdi(prodi);
            }
        }

        if (role.getNamaRole().equals("EKSEKUTIF")){
            EksekutifModel eksekutif = eksekutifService.getEksekutifById(user.getId());
            listRequest = eksekutif.getListPermintaanSurat();
        }

        if (role.getNamaRole().equals("MAHASISWA")) {
            MahasiswaModel mhs = mahasiswaService.getMahasiswabyID(user.getId());
            listRequest = permintaanSuratService.getListPermintaanSuratMahasiswa(mhs);
            Collections.reverse(listRequest);
            model.addAttribute("listRequest", listRequest);
            return "list-request-surat-mahasiswa";
        }

        Collections.reverse(listRequest);
        model.addAttribute("listRequest", listRequest);
        return "list-request-surat";
    }

    @GetMapping("/detailSurat/{idSurat}")
    public String viewDetailSurat(
            @PathVariable Long idSurat,
            HttpServletRequest request,
            Model model
    ) {
        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());
        RoleModel role = user.getRole();
        PermintaanSuratModel surat = permintaanSuratService.getPermintaanSuratById(idSurat);
        List<List<FieldModel>> pairField = new ArrayList<>();
        List<List<FieldModel>> pairFieldMahasiswa = new ArrayList<>();
        List<FieldModel> textFields = surat.getFields();
        List<List<ListFieldForm>> tables = new ArrayList<>();

        for (int i = 1; i < textFields.size(); i+= 2) {
            List<FieldModel> pair = new ArrayList<>();
            pair.add(textFields.get(i));
            if (i+1 < textFields.size()){
                pair.add(textFields.get(i+1));
            }
            pairField.add(pair);
        }

        int listCount = 0;

        if (surat.getFieldlists().size() != 0) {
            int idx = 0;
            tables.add(new ArrayList<>());
            for (FieldListModel fieldList : surat.getFieldlists()) {
                if (fieldList.getListId() != idx) {
                    idx++;
                    tables.add(new ArrayList<>());
                }
                tables.get(idx).add(new ListFieldForm(fieldList.getName(), idx, fieldList.getValue()));
            }
            listCount = idx + 1;
        }
        ToEksekutif toEksekutif = new ToEksekutif();
        List<RiwayatModel> listRiwayat = surat.getRiwayat();
        if (role.getNamaRole().equals("ADMIN")) {
            List<EksekutifModel> listEksekutif = eksekutifService.getAllEksekutif();
            List<EksekutifModel> listEksekutifSurat = surat.getListEksekutif();
            if (listEksekutifSurat.size() != 0){
                EksekutifModel eksekutifAkhir = listEksekutifSurat.get(listEksekutifSurat.size()-1);
                model.addAttribute("eksekutifAkhir", eksekutifAkhir);
            }
            model.addAttribute("listEksekutif", listEksekutif);
            model.addAttribute( "listRiwayat", listRiwayat);
            model.addAttribute( "textFields",textFields);
            model.addAttribute( "tables",tables);
            model.addAttribute( "pair",pairField);
            model.addAttribute( "listCount",listCount);
            model.addAttribute("toEksekutif", toEksekutif);
            model.addAttribute( "surat", surat);

            return "view-detail-surat-admin";
        }
        else if (role.getNamaRole().equals("MAHASISWA")){
            for (int i = 0; i < textFields.size() - 1; i+= 2) {
                List<FieldModel> pair = new ArrayList<>();
                pair.add(textFields.get(i));
                if (textFields.get(i+1) != null){
                    pair.add(textFields.get(i+1));
                }
                pairFieldMahasiswa.add(pair);
            }
            model.addAttribute( "listRiwayat", listRiwayat);
            model.addAttribute( "surat", surat);
            model.addAttribute( "pairField", pairFieldMahasiswa);
            model.addAttribute( "textFields",textFields);
            model.addAttribute( "tables",tables);
            model.addAttribute( "listCount",listCount);
            return "view-detail-surat";
        } else {
            model.addAttribute( "listRiwayat", listRiwayat);
            model.addAttribute( "textFields",textFields);
            model.addAttribute( "tables",tables);
            model.addAttribute( "pair",pairField);
            model.addAttribute( "listCount",listCount);
            model.addAttribute( "surat", surat);

        }
       return "view-detail-surat-eks";
    }

    //Delete permintaan surat
    @GetMapping(value = { "/detailSurat/delete/", "/detailSurat/delete/{idSurat}"})
    public String deletePermintaanSurat(
            @PathVariable Long idSurat,
            HttpServletRequest request,
            Model model
    ){
        try {
            PermintaanSuratModel surat = permintaanSuratService.getPermintaanSuratById(idSurat);

            if (surat !=null) {
                permintaanSuratService.deleteSurat(surat);
                model.addAttribute("pesan", "Surat dengan id " + idSurat + " berhasil dihapus");
                model.addAttribute("surat", surat);
                return "delete-surat";
            } else {
                model.addAttribute("pesan", "Surat dengan id " + idSurat + " tidak tersedia");
                return "error";
            }

        } catch (Exception e){
            model.addAttribute( "pesan", "Proses tidak dapat dilakukan");
            return "error";
        }
    }

    @PostMapping("/detailSurat/{idSurat}/teruskan")
    public ModelAndView teruskan(
            @PathVariable Long idSurat,
            Model model, @ModelAttribute ToEksekutif toEksekutif, HttpServletRequest request
    ) throws MessagingException {

        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());

        Byte status = (byte) 1;
        PermintaanSuratModel surat = permintaanSuratService.getPermintaanSuratById(idSurat);
        RiwayatModel riwayat = new RiwayatModel();
        riwayat.setPermintaanSurat(surat);
        riwayat.setPengubah(user.getUsername());
        riwayat.setStatus(status);
        riwayat.setTanggalWaktu(LocalDateTime.now());
        riwayatService.addRiwayat(riwayat);

        surat.setStatus(status);
        Long idEksekutif = toEksekutif.getIdEksekutif();
        EksekutifModel eksekutif = eksekutifService.getEksekutifById(idEksekutif);

        if (surat.getListEksekutif() == null){
            List<EksekutifModel> listEksekutif = new ArrayList<>();
            listEksekutif.add(eksekutif);
            surat.setListEksekutif(listEksekutif);
        } else {
            surat.getListEksekutif().add(eksekutif);
        }

        List<PermintaanSuratModel> listRequest = permintaanSuratService.getListPermintaanSurat();
        permintaanSuratService.updateSurat(surat);
        try {
            emailService.sendMail(eksekutif,surat, eksekutif.getRole());
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("listRequest", listRequest);
        return new ModelAndView("redirect:" + Setting.CLIENT_BASE_URL + "/daftarSurat");
    }

    @GetMapping("/detailSurat/{idSurat}/alasanPenolakan")
    public String formTolakSurat(
            @PathVariable Long idSurat,
            Model model
    ){
        PermintaanSuratModel surat = permintaanSuratService.getPermintaanSuratById(idSurat);
        model.addAttribute("surat", surat);
        return "form-penolakan";
    }

    @PostMapping("/detailSurat/{idSurat}/tolak")
    public ModelAndView tolakSurat(
            @PathVariable Long idSurat, HttpServletRequest request,
            PermintaanSuratModel suratOld,
            Model model
    ) throws MessagingException {
        Principal principal = request.getUserPrincipal();

        UserModel user = userService.getByUsername(principal.getName());
        PermintaanSuratModel surat = permintaanSuratService.getPermintaanSuratById(idSurat);

        String alasanPenolakan = suratOld.getAlasanPenolakan();
        String alasanFix = "";
        if (alasanPenolakan.length() > 1){
            String[] alasan = alasanPenolakan.split(",");
            if (alasan[0].equals("1")){
                alasanFix = "Dokumen Tidak Lengkap";
            } else if (alasan[0].equals("2")){
                alasanFix = " Terdapat Kesalahan Data";
            } else if (alasan[0].equals("3")) {
                alasanFix = "Permintaan Tidak Dapat Dipenuhi";
            } else {
                alasanFix =  "Isi Tidak Sesuai";
            }
            alasanFix = alasanFix + ": " + alasan[1];
        }
        Byte status = (byte) 4;
        surat.setAlasanPenolakan(alasanFix);
        surat.setStatus(status);
        LocalDate now = LocalDate.now();
        surat.setTanggalSelesai(now);
        Long days = ChronoUnit.DAYS.between(surat.getTanggalPembuatan(),now);
        Integer countDays = days.intValue();
        surat.setDurasi(countDays);
        permintaanSuratService.updateSurat(surat);

        RiwayatModel riwayat = new RiwayatModel();
        riwayat.setPermintaanSurat(surat);
        riwayat.setPengubah(user.getUsername());
        riwayat.setStatus(status);
        riwayat.setTanggalWaktu(LocalDateTime.now());
        riwayatService.addRiwayat(riwayat);

        try {
            emailService.sendMail(surat.getMahasiswa(), surat, surat.getMahasiswa().getRole());
            for (AdminModel admin : adminService.getAllAdminByProdi(surat.getProdiMahasiswa())
            ) {
                emailService.sendMail(admin, surat, admin.getRole());

            }

            for(AdminModel adminFakultas : adminService.getAllAdminByProdi("ADMIN FAKULTAS")){
                emailService.sendMail(adminFakultas, surat, adminFakultas.getRole());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("redirect:" + Setting.CLIENT_BASE_URL + "/daftarSurat");
    }

    @GetMapping("/detailSurat/{idSurat}/setujui")
    public ModelAndView persetujuanEksekutif(
            @PathVariable Long idSurat,HttpServletRequest request,
            Model model
    ) throws MessagingException {
        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());

        Byte status = (byte) 2;
        PermintaanSuratModel surat = permintaanSuratService.getPermintaanSuratById(idSurat);
        surat.setStatus(status);
        permintaanSuratService.updateSurat(surat);

        RiwayatModel riwayat = new RiwayatModel();
        riwayat.setPermintaanSurat(surat);
        riwayat.setPengubah(user.getUsername());
        riwayat.setStatus(status);
        riwayat.setTanggalWaktu(LocalDateTime.now());
        riwayatService.addRiwayat(riwayat);

        try{
            for (AdminModel admin : adminService.getAllAdminByProdi(surat.getProdiMahasiswa())
            ) {
                emailService.sendMail(admin, surat, admin.getRole());

            }

            for(AdminModel adminFakultas : adminService.getAllAdminByProdi("ADMIN FAKULTAS")){
                emailService.sendMail(adminFakultas, surat, adminFakultas.getRole());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<PermintaanSuratModel> listRequest = new ArrayList<>();
        if (user.getRole().getNamaRole().equals("ADMIN")){
            listRequest = permintaanSuratService.getListPermintaanSurat();
        }

        if (user.getRole().getNamaRole().equals("EKSEKUTIF")){
            EksekutifModel eksekutif = eksekutifService.getEksekutifById(user.getId());
            listRequest = eksekutif.getListPermintaanSurat();

        }
        model.addAttribute("listRequest", listRequest);
        return new ModelAndView("redirect:" + Setting.CLIENT_BASE_URL + "/daftarSurat");
    }

    @GetMapping("/detailSurat/{idSurat}/selesai")
    public ModelAndView suratSelesai(
            @PathVariable Long idSurat,HttpServletRequest request,
            Model model, @ModelAttribute PermintaanSuratModel suratOld
    ) throws MessagingException {
        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());

        Byte status = (byte) 3;
        PermintaanSuratModel surat = permintaanSuratService.getPermintaanSuratById(idSurat);
        surat.setStatus(status);
        LocalDate now = LocalDate.now();
        surat.setTanggalSelesai(now);
        Long days = ChronoUnit.DAYS.between(surat.getTanggalPembuatan(),now);
        Integer countDays = days.intValue();
        surat.setDurasi(countDays);
        permintaanSuratService.updateSurat(surat);

        FileModel file = fileService.getFileByPermintaanSurat(surat);
        RiwayatModel riwayat = new RiwayatModel();
        riwayat.setPermintaanSurat(surat);
        riwayat.setPengubah(user.getUsername());
        riwayat.setStatus(status);
        riwayat.setTanggalWaktu(LocalDateTime.now());
        riwayatService.addRiwayat(riwayat);
        surat.setStatus(status);

        permintaanSuratService.updateSurat(surat);
        try {
            emailService.sendMail(surat.getMahasiswa(), surat, surat.getMahasiswa().getRole());
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<PermintaanSuratModel> listRequest = permintaanSuratService.getListPermintaanSurat();
        model.addAttribute("file", file);
        model.addAttribute("listRequest", listRequest);


        return new ModelAndView("redirect:" + Setting.CLIENT_BASE_URL + "/daftarSurat");
    }

    @GetMapping("/daftar-template-surat")
    public String daftarTemplateSurat(
            HttpServletRequest request,
            Model model
    ) {
        UserModel user = userService.getByUsername(request.getUserPrincipal().getName());
        List<JenisSuratModel> listJenisSurat= jenisSuratService.getListJenisSuratByProdi(user.getProdi());
        model.addAttribute("listJenisSurat", listJenisSurat);
        return "halaman-jenis-surat";
    }

    @GetMapping("/addSurat/add")
    public String formSurat(
            @RequestParam(value = "id") Integer id,
            HttpServletRequest request,
            Model model
    ) {
        JenisSuratModel jenisSurat = jenisSuratService.getSuratById(id);
        SuratForm suratForm = new SuratForm();

        suratForm.setJenisSurat(jenisSurat);

        List<FieldAndTypeModel> fieldList = jenisSurat.getFieldsAndTypes();

        int listCount = 0;
        for (FieldAndTypeModel field : fieldList) {
            if (field.getType().equals("list")) {
                if (suratForm.getListFields() == null) {
                    suratForm.setListFields(new ArrayList<>());
                }
                List<String> columnNames = Arrays.asList(field.getName().split(","));
                for (String columnName : columnNames) {
                    suratForm.getListFields().add(new ArrayList<>());
                    suratForm.getListFields().get(listCount).add(new ListFieldForm(columnName, listCount));
                }
                listCount++;
            } else {
                if (suratForm.getTextFields() == null) {
                    suratForm.setTextFields(new ArrayList<>());
                }
                suratForm.getTextFields().add(new FieldForm(field.getName(), field.getType()));
            }
        }

        suratForm.setListCount(listCount);

        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());
        MahasiswaModel mhs = mahasiswaService.getMahasiswabyID(user.getId());

        model.addAttribute("suratForm",suratForm);
        model.addAttribute("mahasiswa", mhs);
        return "form-request-surat";
    }

    @PostMapping(
            value = "/addSurat/add",
            params = {"simpan"}
    )
    public ModelAndView tambahPermintaanSurat(
            HttpServletRequest request,
            @ModelAttribute SuratForm suratForm

    ){

        PermintaanSuratModel suratNew = new PermintaanSuratModel();
        JenisSuratModel jenisSurat = jenisSuratService.getSuratById(suratForm.getJenisSurat().getId());
        suratNew.setJenisSurat(jenisSurat);

        Byte status = (byte) 0;
        suratNew.setStatus(status);

        suratNew.setBentukSurat(suratForm.getBentukSurat().byteValue());

        suratNew.setBahasaSurat(suratForm.getBahasaSurat());

        suratNew.setTanggalPembuatan(LocalDate.now());

        suratNew.setKeperluan(suratForm.getKeperluan());

        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());
        MahasiswaModel mhs = mahasiswaService.getMahasiswabyID(user.getId());
        suratNew.setMahasiswa(mhs);
        suratNew.setProdiMahasiswa(mhs.getProdi());

        if (suratForm.getTextFields() != null) {
            suratNew.setFields(new ArrayList<>());
            for (FieldForm field : suratForm.getTextFields()) {
                suratNew.getFields().add(new FieldModel(field.getName(), field.getValue(), suratNew));
            }
        }

        if (suratForm.getListFields() != null) {
            suratNew.setFieldlists(new ArrayList<>());
            int tableIdx = 0;
            for (List<ListFieldForm> table : suratForm.getListFields()) {
                for (ListFieldForm field : table) {
                    List <String> formValues = field.getValues();
                    StringBuilder values = new StringBuilder(formValues.get(0));
                    for (int i = 1; i < formValues.size(); i++) {
                        values.append(",").append(formValues.get(i));
                    }
                    suratNew.getFieldlists().add(new FieldListModel(field.getName(), values.toString(), tableIdx, suratNew));
                }
                tableIdx++;
            }
        }

        permintaanSuratService.addSurat(suratNew);

        RiwayatModel riwayat = new RiwayatModel();
        riwayat.setPermintaanSurat(suratNew);
        riwayat.setPengubah(user.getUsername());
        riwayat.setStatus(status);
        riwayat.setTanggalWaktu(LocalDateTime.now());
        riwayatService.addRiwayat(riwayat);

        try {
            for (AdminModel admin : adminService.getAllAdminByProdi(suratNew.getProdiMahasiswa())
            ) {
                sendMail(admin, suratNew, admin.getRole());
            }

            for(AdminModel adminFakultas : adminService.getAllAdminByProdi("ADMIN FAKULTAS")){
                emailService.sendMail(adminFakultas, suratNew, adminFakultas.getRole());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ModelAndView("redirect:" + Setting.CLIENT_BASE_URL + "/daftarSurat");
    }

    @PostMapping(
            value = {"/addSurat/add", "/updateSurat/{id}"},
            params = {"tambahField"}
    )
    private String tambahFieldTambahPermintaan(
            @ModelAttribute SuratForm suratForm,
            HttpServletRequest request,
            @PathVariable(required = false) Long id,
            @RequestParam("tambahField") Integer tableIdx,
            Model model
    ) {
        for (ListFieldForm listFieldForm : suratForm.getListFields().get(tableIdx)) {
            listFieldForm.getValues().add("");
        }

        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());
        MahasiswaModel mhs = mahasiswaService.getMahasiswabyID(user.getId());

        model.addAttribute("suratForm", suratForm);
        model.addAttribute("mahasiswa", mhs);

        if (id != null) {
            model.addAttribute("idPermintaanSurat", id);
            return "form-update-request-surat";
        }

        return "form-request-surat";
    }

    @PostMapping(
            value = {"/addSurat/add", "/updateSurat/{id}"},
            params = {"hapusField"}
    )
    private String hapusFieldTambahPermintaan(
            @ModelAttribute SuratForm suratForm,
            @RequestParam("hapusField") List<String> indexes,
            @PathVariable(required = false) Long id,
            HttpServletRequest request,
            Model model
    ) {
        Integer fieldIdx = Integer.valueOf(indexes.get(0));
        Integer tableIdx = Integer.valueOf(indexes.get(1));
        for (ListFieldForm listFieldForm : suratForm.getListFields().get(tableIdx)) {
            listFieldForm.getValues().remove(fieldIdx.intValue());
        }

        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());
        MahasiswaModel mhs = mahasiswaService.getMahasiswabyID(user.getId());

        model.addAttribute("suratForm", suratForm);
        model.addAttribute("mahasiswa", mhs);

        if (id != null) {
            model.addAttribute("idPermintaanSurat", id);
            return "form-update-request-surat";
        }

        return "form-request-surat";
    }

    @GetMapping(
            value = "/updateSurat/{id}"
    )
    private String formUpdatePermintaanSurat(
            @PathVariable Long id,
            Model model
    ) {
        PermintaanSuratModel permintaanSurat = permintaanSuratService.getPermintaanSuratById(id);
        SuratForm suratForm = new SuratForm();

        suratForm.setJenisSurat(permintaanSurat.getJenisSurat());
        suratForm.setBahasaSurat(permintaanSurat.getBahasaSurat());
        suratForm.setBentukSurat(permintaanSurat.getBentukSurat().intValue());
        suratForm.setKeperluan(permintaanSurat.getKeperluan());

        int idx = 0;
        if (permintaanSurat.getFields().size() != 0) {
            suratForm.setTextFields(new ArrayList<>());
            for (FieldModel field : permintaanSurat.getFields()) {
                suratForm.getTextFields().add(
                        new FieldForm(
                                field.getName(),
                                field.getValue(),
                                permintaanSurat
                                        .getJenisSurat()
                                        .getFieldsAndTypes()
                                        .get(idx)
                                        .getType()
                        )
                );
                idx++;
            }
        }

        if (permintaanSurat.getFieldlists().size() != 0) {
            suratForm.setListFields(new ArrayList<>());
            idx = 0;
            suratForm.getListFields().add(new ArrayList<>());
            for (FieldListModel fieldList : permintaanSurat.getFieldlists()) {
                if (fieldList.getListId() != idx) {
                    idx++;
                    suratForm.getListFields().add(new ArrayList<>());
                }
                suratForm.getListFields().get(idx).add(new ListFieldForm(fieldList.getName(), idx, fieldList.getValue()));
            }
            suratForm.setListCount(idx + 1);
        }

        MahasiswaModel mhs = permintaanSurat.getMahasiswa();

        model.addAttribute("suratForm",suratForm);
        model.addAttribute("mahasiswa", mhs);
        model.addAttribute("idPermintaanSurat", id);
        return "form-update-request-surat";
    }

    @PostMapping(
            value = "/updateSurat/{id}",
            params = {"simpan"}
    )
    private ModelAndView updatePermintaanSurat(
            @PathVariable Long id,
            @ModelAttribute SuratForm suratForm,
            HttpServletRequest request
    ) {

        PermintaanSuratModel surat = permintaanSuratService.getPermintaanSuratById(id);

        Byte status = (byte) 0;
        surat.setStatus(status);

        surat.setBentukSurat(suratForm.getBentukSurat().byteValue());

        surat.setBahasaSurat(suratForm.getBahasaSurat());

        surat.setTanggalPembuatan(LocalDate.now());

        surat.setKeperluan(suratForm.getKeperluan());

        if (suratForm.getTextFields() != null) {
            for (int i = 0 ; i < surat.getFields().size(); i++) {
                surat
                        .getFields()
                        .get(i)
                        .setValue(
                                suratForm
                                        .getTextFields()
                                        .get(i)
                                        .getValue()
                        );
            }
        }

        if (suratForm.getListFields() != null) {
            int idx = 0;
            for (List<ListFieldForm> table : suratForm.getListFields()){
                for (ListFieldForm field : table) {
                    List <String> formValues = field.getValues();
                    StringBuilder values = new StringBuilder(formValues.get(0));
                    for (int i = 1; i < formValues.size(); i++) {
                        values.append(",").append(formValues.get(i));
                    }
                    surat.getFieldlists().get(idx++).setValue(values.toString());
                }
            }
        }
        permintaanSuratService.updateSurat(surat);

        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());

        RiwayatModel riwayat = new RiwayatModel();
        riwayat.setPermintaanSurat(surat);
        riwayat.setPengubah(user.getUsername());
        riwayat.setStatus(status);
        riwayat.setTanggalWaktu(LocalDateTime.now());
        riwayatService.addRiwayat(riwayat);

        return new ModelAndView("redirect:" + Setting.CLIENT_BASE_URL + "/daftarSurat");
    }

    public void sendMail(UserModel user, PermintaanSuratModel surat, RoleModel role) throws MessagingException {
        emailService.sendMail(user, surat, role) ;
    }

    @GetMapping("/statistik")
    public String dashboard(Model model, HttpServletRequest request){
        Principal principal = request.getUserPrincipal();
        UserModel user = userService.getByUsername(principal.getName());
        RoleModel role = user.getRole();
        List<PermintaanSuratModel> listpermintaansurat = permintaanSuratService.getListPermintaanSurat();
        int suratMasuk = listpermintaansurat.size();
        int suratDiproses = 0;
        int suratSelesai = 0;
        int suratDitolak = 0;
        int suratTerlambat = 0;
        int suratTepatWaktu = 0;
        for(int i=0; i<listpermintaansurat.size(); i++){
            if(listpermintaansurat.get(i).getStatus() == 1 || listpermintaansurat.get(i).getStatus() == 2 ){
                suratDiproses++;
            }

            else if(listpermintaansurat.get(i).getStatus() == 3){
                suratSelesai++;
            }
            else if(listpermintaansurat.get(i).getStatus() == 4){
                suratDitolak++;
            }
        }

        for(int i = 0; i<listpermintaansurat.size(); i++) {
            if (listpermintaansurat.get(i).getStatus() == 3) {

                if (listpermintaansurat.get(i).getDurasi() > listpermintaansurat.get(i).getJenisSurat().getBatasWaktu()) {
                    suratTerlambat++;
                }
                else if (listpermintaansurat.get(i).getDurasi() <= listpermintaansurat.get(i).getJenisSurat().getBatasWaktu()) {
                    suratTepatWaktu++;
                }
            }
        }


        model.addAttribute("suratMasuk", suratMasuk);
        model.addAttribute("suratSelesai", suratSelesai);
        model.addAttribute("suratDitolak", suratDitolak);
        model.addAttribute("suratDiproses", suratDiproses);
        model.addAttribute("suratTerlambat", suratTerlambat);
        model.addAttribute("suratTepatWaktu", suratTepatWaktu);
        return "statistik";
    }


    @GetMapping("/upload/file/{id}")
    private String addBerkasForm (
            @PathVariable Long id,
            Model model
    ) {
        PermintaanSuratModel permintaanSurat = permintaanSuratService.getPermintaanSuratById(id);
        FileModel fileModel = permintaanSurat.getFile();
        model.addAttribute("fileModel", fileModel);
        model.addAttribute("idPermintaanSurat", id);
        return "upload-berkas";
    }

    @RequestMapping(value=("/upload/file"), headers=("content-type=multipart/*"))
    public ModelAndView addBerkasPost(
            @RequestParam(value = "file") MultipartFile fileBerkas,
            @RequestParam(value = "idPermintaanSurat") Long idPermintaanSurat,
            HttpServletRequest httpReq,
            RedirectAttributes attributes
    ) {
        ModelAndView mav;
        httpReq.setAttribute("idPermintaanSurat", idPermintaanSurat);
        try {
            if(!FilenameUtils.getExtension(fileBerkas.getOriginalFilename()).equals("pdf")) {
                mav = new ModelAndView("redirect:/detailSurat/" + idPermintaanSurat);
                attributes.addFlashAttribute("alert", "PERINGATAN: Hanya masukkan berkas yang berekstensi zip!");
            } else if (fileBerkas.getSize() > 10000000){
                mav = new ModelAndView("redirect:/detailSurat/" + idPermintaanSurat);
                attributes.addFlashAttribute("alert", "PERINGATAN: Maksimum ukuran berkas yang bisa diunggah adalah 10MB!");
            } else {
                fileService.saveFile(fileBerkas, idPermintaanSurat);
                attributes.addFlashAttribute("alert", "Surat sudah terupload!");
                mav = new ModelAndView("redirect:/detailSurat/" + idPermintaanSurat);
            }
        } catch (IOException e) {
            mav = new ModelAndView("redirect:/detailSurat/" + idPermintaanSurat);
            attributes.addFlashAttribute("alert", "PERINGATAN: Maksimum ukuran berkas yang bisa diunggah adalah 10MB!");
        }
        return mav;
    }

    @GetMapping("/permintaanSurat/download/{idPermintaanSurat}")
    public ResponseEntity<byte[]> download(
            @PathVariable Long idPermintaanSurat
    ) throws IOException {
        PermintaanSuratModel surat = permintaanSuratService.getPermintaanSuratById(idPermintaanSurat);
        FileModel file = fileService.getFileByPermintaanSurat(surat);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getNama() + "\"")
                .body(file.getFile());
    }
}
