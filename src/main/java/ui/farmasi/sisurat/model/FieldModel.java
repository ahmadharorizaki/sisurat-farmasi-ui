package ui.farmasi.sisurat.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "field")
public class FieldModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String value;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn (referencedColumnName = "idPermintaanSurat")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PermintaanSuratModel permintaanSurat;

    public FieldModel(){
    }

    public FieldModel(String name, String value, PermintaanSuratModel permintaanSurat) {
        this.name = name;
        this.value = value;
        this.permintaanSurat = permintaanSurat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String field) {
        this.name = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PermintaanSuratModel getPermintaanSurat() {
        return permintaanSurat;
    }

    public void setPermintaanSurat(PermintaanSuratModel permintaanSurat) {
        this.permintaanSurat = permintaanSurat;
    }

}
