package ui.farmasi.sisurat.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class EksekutifModel extends UserModel {

    @ManyToMany(mappedBy = "listEksekutif")
    List<PermintaanSuratModel> listPermintaanSurat;

    public List<PermintaanSuratModel> getListPermintaanSurat() {
        return listPermintaanSurat;
    }

    public void setListPermintaanSurat(List<PermintaanSuratModel> listEksekutif) {
        this.listPermintaanSurat = listEksekutif;
    }
}
