package ui.farmasi.sisurat.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.File;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="File")

public class FileModel implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="file", nullable = true)
    private byte[] file;

    @Column(name="nama", nullable = true)
    private String nama;

    @Column(name="tipeFile", nullable = true)
    private String tipe;

    public FileModel(){}

    public FileModel(String name, String type, byte[] data) {
        this.nama = name;
        this.tipe = type;
        this.file = data;
    }

    @OneToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="permintaanSurat", referencedColumnName = "idPermintaanSurat")
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PermintaanSuratModel permintaanSurat;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public PermintaanSuratModel getPermintaanSurat() {
        return permintaanSurat;
    }

    public void setPermintaanSurat(PermintaanSuratModel permintaanSurat) {
        this.permintaanSurat = permintaanSurat;
    }
}

