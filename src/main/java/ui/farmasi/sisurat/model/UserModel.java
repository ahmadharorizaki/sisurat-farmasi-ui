package ui.farmasi.sisurat.model;

import com.sun.istack.NotNull;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false)
    private String username;

    @NotNull
    @Column(nullable = false)
    private String prodi;

    private String emailUI;

    @Email(message = "Email tidak valid", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    private String emailNonUI;

    private String password;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @OnDelete(action = OnDeleteAction.CASCADE)
    private RoleModel role;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmailUI() {
        return emailUI;
    }

    public String getEmailNonUI() {
        return emailNonUI;
    }

    public RoleModel getRole() {
        return role;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmailUI(String emailUI) {
        this.emailUI = emailUI;
    }

    public void setEmailNonUI(String emailNonUI) {this.emailNonUI = emailNonUI;}

    public void setRole(RoleModel role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProdi() { return prodi; }

    public void setProdi(String prodi) { this.prodi = prodi; }
}
