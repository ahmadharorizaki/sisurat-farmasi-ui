package ui.farmasi.sisurat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ui.farmasi.sisurat.model.FieldModel;
import ui.farmasi.sisurat.model.JenisSuratModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;

import java.util.Optional;

@Repository
public interface FieldDB extends JpaRepository<FieldModel, Long> {
}
