package ui.farmasi.sisurat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ui.farmasi.sisurat.model.MahasiswaModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface MahasiswaDb extends JpaRepository<MahasiswaModel, Long> {
    MahasiswaModel findMahasiswaModelById (Long id);
    List<MahasiswaModel> findMahasiswaModelByNameContainsIgnoreCase(String search);
    MahasiswaModel findMahasiswaModelBynpm(String search);
}
