package ui.farmasi.sisurat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ui.farmasi.sisurat.model.EksekutifModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;

import java.util.List;

@Repository
public interface EksekutifDb extends JpaRepository<EksekutifModel, Long> {
    List<EksekutifModel> findAll();
    EksekutifModel getEksekutifModelById(Long id);
}



