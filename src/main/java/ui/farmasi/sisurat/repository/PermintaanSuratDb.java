package ui.farmasi.sisurat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ui.farmasi.sisurat.model.JenisSuratModel;
import ui.farmasi.sisurat.model.MahasiswaModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface PermintaanSuratDb extends JpaRepository<PermintaanSuratModel, Long> {
    List<PermintaanSuratModel> findAllByMahasiswa(MahasiswaModel mhs);
    Optional<PermintaanSuratModel> findByIdPermintaanSurat(Long id);
    List<PermintaanSuratModel> findAll();
    List<PermintaanSuratModel> findAllByStatus(Byte status);
    List<PermintaanSuratModel> findAllByProdiMahasiswa(String prodi);
    void deleteAllByMahasiswa(MahasiswaModel mahasiswaModel);
}


