package ui.farmasi.sisurat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ui.farmasi.sisurat.model.RoleModel;

@Repository
public interface RoleDb extends JpaRepository<RoleModel, Integer> {

    RoleModel findByNamaRole(String namaRole);
}
