package ui.farmasi.sisurat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ui.farmasi.sisurat.model.JenisSuratModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface JenisSuratDb extends JpaRepository<JenisSuratModel, Long> {
    Optional<JenisSuratModel> findById(Integer id);
    List<JenisSuratModel> findAllByProdi(String prodi);
}
