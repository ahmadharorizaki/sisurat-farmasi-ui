package ui.farmasi.sisurat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ui.farmasi.sisurat.model.FieldAndTypeModel;

@Repository
public interface FieldAndTypeDb extends JpaRepository<FieldAndTypeModel, Long> {

}
