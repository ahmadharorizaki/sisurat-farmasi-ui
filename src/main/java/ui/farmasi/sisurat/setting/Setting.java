package ui.farmasi.sisurat.setting;

public class Setting {
    final public static String CLIENT_BASE_URL = "https://sisurat-farmasi-ui.herokuapp.com";
    final public static String CLIENT_LOGIN = CLIENT_BASE_URL + "/validate-ticket";
    final public static String CLIENT_LOGOUT = CLIENT_BASE_URL + "/logout";

    final public static String SERVER_BASE_URL = "https://sso.ui.ac.id";
    final public static String SERVER_LOGIN = SERVER_BASE_URL + "/cas2/login?service=" ;
    final public static String SERVER_LOGOUT = SERVER_BASE_URL + "/cas2/logout?url=";
    final public static String SERVER_VALIDATE_TICKET
            = SERVER_BASE_URL + "/cas2/serviceValidate?ticket=%s&service=%s";
}
