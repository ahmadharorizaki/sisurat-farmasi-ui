package ui.farmasi.sisurat.rest;

import ui.farmasi.sisurat.model.MahasiswaModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;
import ui.farmasi.sisurat.model.RiwayatModel;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PermintaanSuratDTO {
    public String jenisSurat;
    public String waktuDiajukan;
    public Long idPermintaanSurat;
    public String status;
    public String bentukSurat;
    public String bahasaSurat;
    public String nama;

    public static PermintaanSuratDTO convertToDTO (PermintaanSuratModel surat) {
        PermintaanSuratDTO newSurat = new PermintaanSuratDTO();
        newSurat.idPermintaanSurat = surat.getIdPermintaanSurat();
        newSurat.nama = surat.getMahasiswa().getName();
        newSurat.jenisSurat = surat.getJenisSurat().getNama();

        for (RiwayatModel a :surat.getRiwayat()
             ) {
            if (a.getStatus() == 0){
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy ");
                String formatDateTime = a.getTanggalWaktu().format(formatter);
                newSurat.waktuDiajukan = formatDateTime;
            }
        }

        if (surat.getBahasaSurat() == 0){
            newSurat.bahasaSurat = "Indonesia";
        } else {
            newSurat.bahasaSurat = "Inggris";
        }

        if (surat.getBentukSurat() == 0){
            newSurat.bentukSurat = "SoftCopy";
        } else {
            newSurat.bentukSurat = "HardCopy";
        }

        if(surat.getStatus() == 0){
            newSurat.status = "Diajukan";
        } else if (surat.getStatus() == 1){
            newSurat.status = "Menunggu Persetujuan";
        } else if (surat.getStatus() == 2){
            newSurat.status = "Disetujui";
        } else if (surat.getStatus() == 3){
            newSurat.status = "Selesai";
        } else {
            newSurat.status = "Ditolak";
        }
        return newSurat;
    }

}
