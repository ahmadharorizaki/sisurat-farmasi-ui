package ui.farmasi.sisurat.rest;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Setter
@Getter
@XmlAccessorType(XmlAccessType.FIELD)
public class Attributes {

    @XmlElement(namespace = "http://www.yale.edu/tp/cas")
    private String ldap_cn;

    @XmlElement(namespace = "http://www.yale.edu/tp/cas")
    private String kd_org;

    @XmlElement(namespace = "http://www.yale.edu/tp/cas")
    private String peran_user;

    @XmlElement(namespace = "http://www.yale.edu/tp/cas")
    private String nama;

    @XmlElement(namespace = "http://www.yale.edu/tp/cas")
    private String npm;

    public String getLdap_cn() {
        return ldap_cn;
    }

    public void setLdap_cn(String ldap_cn) {
        this.ldap_cn = ldap_cn;
    }

    public String getKd_org() {
        return kd_org;
    }

    public void setKd_org(String kd_org) {
        this.kd_org = kd_org;
    }

    public String getPeran_user() {
        return peran_user;
    }

    public void setPeran_user(String peran_user) {
        this.peran_user = peran_user;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }
}
