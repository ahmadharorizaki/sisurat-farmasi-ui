package ui.farmasi.sisurat.rest;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Setter
@Getter
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthenticationSuccess {

    @XmlElement(name = "user", namespace = "http://www.yale.edu/tp/cas")
    private String user;

    @XmlElement(name="attributes", namespace = "http://www.yale.edu/tp/cas")
    private Attributes attributes;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }
}
