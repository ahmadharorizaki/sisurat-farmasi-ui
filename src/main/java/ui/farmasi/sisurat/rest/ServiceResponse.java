package ui.farmasi.sisurat.rest;


import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@XmlRootElement(name = "serviceResponse", namespace = "http://www.yale.edu/tp/cas")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceResponse{

    @XmlElement(name ="authenticationFailure" ,namespace = "http://www.yale.edu/tp/cas")
    private String authenticationFailure;

    @XmlElement(name ="authenticationSuccess" ,namespace = "http://www.yale.edu/tp/cas")
    private AuthenticationSuccess authenticationSuccess;

    public String getAuthenticationFailure() {
        return authenticationFailure;
    }

    public void setAuthenticationFailure(String authenticationFailure) {
        this.authenticationFailure = authenticationFailure;
    }

    public AuthenticationSuccess getAuthenticationSuccess() {
        return authenticationSuccess;
    }

    public void setAuthenticationSuccess(AuthenticationSuccess authenticationSuccess) {
        this.authenticationSuccess = authenticationSuccess;
    }
}
